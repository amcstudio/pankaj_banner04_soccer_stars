'use strict';
~ function() {
    var $ = TweenMax,
    	power1InOut = Power1.easeInOut,
        ad = document.getElementById('mainContent'),
        ground = document.getElementById('groundContainer'),
        football = document.getElementById('football'),
        ball = document.getElementById('ball'),
        footballSpritWidth = 2600,
        tID; 

    window.init = function() {

        var tl = new TimelineMax();
        tl.set(ad, { perspective: 1000, force3D: true })
        tl.set(ground, { y:-250 })
        tl.set("#copy1", { y: -110 })
        tl.set("#copy2", { y: 110 })
        tl.set(football,{ x: 480,y:-100, onComplete: animateFootball})

        startAnimation();
        
    }
    
    function startAnimation(){
        var tl = new TimelineMax();
        //Frame one
        tl.addLabel("frameOne")
            .to(ground, 0.8, { y: -510, ease: power1InOut },"frameOne")
            .to("#copy1", 0.8, { y: 0, opacity:1, ease: Bounce.easeOut },"frameOne")
            .to(football,0.8, { x: 0,y:0, ease: Sine.easeOut, onComplete: stopFootball},"frameOne")
            
            .to("#copy1", 0.8, { opacity: 0, ease: Sine.easeOut },"frameOne+=2")
            .to(ground, 0.8, { x: -350, y: -350, scale: .85, ease: power1InOut },"frameOne+=2")
            // -530, -190
            // .to(ground, 0.8, { x: -550, y: -280, scale: .6, ease: power1InOut },"frameOne+=2")
            .to("#copy2", 0.8, { y:0, opacity: 1,  ease: Bounce.easeOut },"frameOne+=2.5")
            .to("#playerMark", .5, { opacity: 1,  ease: Sine.easeOut },"frameOne+=3")
            .to("#playerMark", 2, { opacity: 1, rotation: 180, ease: Sine.easeOut },"frameOne+=3.3")
            .to("#playerMark", .5, { opacity: 0,  ease: Sine.easeOut },"frameOne+=4.3")
            
            
            tl.addLabel("frameTwo")
            .to("#frPlayer1", 1, { x: 340, y: -660, ease: Sine.easeOut },"frameTwo")
            .to("#frStricker",1.8, { rotation: 200, ease: Sine.easeOut },"frameTwo")
            // .set(ball, { rotation: 125, onComplete: animateFootball },"frameTwo")
            
            .to("#copy2", 0.5, { opacity:0, ease: Sine.easeOut },"frameTwo+=.1")
            .to(ground, 1, { x: -470, y: -118, ease: Sine.easeInOut },"frameTwo")
            
            .to(football,.6, { x: 470,y:-880, ease: Sine.easeOut},"frameTwo+=.15")
            .set(ball, { rotation: -130},"frameTwo+=.6")
            .to(football, .6, { x: 980,y:-260, ease: Sine.easeInOut },"frameTwo+=.6")
            .to(ground, 0.8, { x: -800, y: -270, scale: .9, ease: Sine.easeInOut },"frameTwo+=1")
            .set(ball, { rotation: -40 },"frameTwo+=7")
            .to(football, .4, { x: 880,y:-140, ease: Sine.easeOut, onComplete: stopFootball  },"frameTwo+=7")
            // .to(ground, 0.8, { y: 0, ease: power1InOut, onComplete: stopFootball},"frameTwo+=2.2")




    }


    function stopFootball() {
        clearInterval(tID);
    }
    function animateFootball() {
        var position = 200,
            interval = 100,
            diff = 200;     
        tID = setInterval ( 
            function() {
                ball.style.backgroundPosition =  -position+'px 0px'; 
                
                if ( position < footballSpritWidth) { 
                    position = position + diff;
                }
                else { 
                    position = 200; 
                }
            } , interval );


            // setTimeout(
            //     function() {
            //         clearInterval(tID);
            //     },1000)
    }


}();
